# CONFORMiT Frontend VueJS test

Le but de ce test est de permettre au candidat de démontrer ses connaissances et son savoir-faire en VueJS et HTML/JS/CSS. Il s'agit de concevoir un mini-projet respectant le cahier des charges et les contraintes définis plus bas. Pour le reste, le candidat a carte blanche pour démontrer ces capacités.

Pour ce test, il est attendu que le candidat consacre environ 2h.



Pour effectuer ce test, trois éléments sont fournis au candidat :

- Ce même fichier contenant les instructions sur ce qui est attendu pour ce test
- Un fichier javascript contenant une fonction renvoyant la liste des commentaires par défaut (data.js)
- Un projet VueJS squelette composé des fichiers de base et d'icones
- Un mockup de ce qui est attendu



Contraintes techniques :

- Le projet doit consisté en une single page application utilisant le framework VueJS [https://vuejs.org/]
- Bien que l'affichage des données doivent être dynamique, il n'est pas attendu que toutes les données soient conservées si je rafraichi la page
- Le design proposé doit etre respecté au maximum (mais sans consacrer trop de temps à l'exactitude)


Cahier des charges :

- Pouvoir consulter une liste de commentaires représentés comme indiqué dans le mockup
- Afficher au dessus de la liste le nombre de commentaires existants sur la page
- Pouvoir supprimer un commentaire
- Pouvoir ajouter un nouveau commentaire
- Pouvoir modifier un commentaire existant
- L'interface doit être responsive
