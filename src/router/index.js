import Vue from 'vue'
import VueRouter from 'vue-router'
import Comments from '@/views/Comments'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Index",
    redirect: { name: "Comments" }
  },
  {
    path: "/comments",
    name: "Comments",
    component: Comments
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
