//  @/store/modules/comments.js
import Vue from 'vue'
import { getComments } from '@/dataSets/data.js'
import _ from 'lodash'

const state = {
    comments: []
}

const getters = {
    count: state => state.comments.length
}

const mutations = {
    ADD_COMMENTS( state, comments ) {
        state.comments = [ ...state.comments, ...comments ]
    },
    ADD_COMMENT( state, comment ) {
        state.comments = [ ...state.comments, comment ]
    },
    DELETE_COMMENT( state, comment ) {
        state.comments = state.comments.filter( x => !_.isEqual( x, comment ) )
    },
    DELETE_COMMENTS( state ) {
        state.comments = []
    },
    UPDATE_COMMENT( state, comments ) {
        Vue.set( 
            state.comments, 
            state.comments.indexOf( comments.originalPost ), 
            comments.updatedPost 
        )
    },
}

const actions = {
    init({ commit }) {        
        commit( 'DELETE_COMMENTS' )
        commit( 'ADD_COMMENTS', getComments() )
    },
    add({ commit }, comment ) {
        commit( 'ADD_COMMENT', comment )
    },
    del({ commit }, comment ) {
        commit( 'DELETE_COMMENT', comment )
    },
    update({ commit }, comments ) {
        commit( 'UPDATE_COMMENT', comments )
    },
}


export default { namespaced: true, state, mutations, getters, actions }