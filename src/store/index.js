import Vue from 'vue'
import Vuex from 'vuex'
Vue.use( Vuex )

import comments from '@/store/modules/comments.js'

const modules = { comments }

const state = {}

const getters = {}

const mutations = {}

const actions = {}



const store = new Vuex.Store({
    state,
    modules,
    mutations,
    actions,
    getters,
})

export default store